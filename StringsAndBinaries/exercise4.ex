defmodule StringsAndBinaries.Exercise4 do
  def calculate(expression) do
    _calculate(term(expression, :left), term(expression, :right), operator(expression))
  end

  defp term(expression, position_sym) do
    terms = Enum.chunk_by(expression, &(&1 in '?\s+-*/'))
    position = case position_sym do
      :left -> 0
      _ -> -1
    end

    terms
    |> Enum.at(position)
    |> List.to_integer
  end

  defp operator(expression) do
    Enum.partition(expression, fn(x) -> x in '+-*/' end)
    |> Tuple.to_list
    |> Enum.at(0)
  end

  defp _calculate(term1, term2, operator) do
    case operator do
      '+' -> term1 + term2
      '-' -> term1 - term2
      '*' -> term1 * term2
      '/' -> term1 / term2
    end
  end
end

128 = StringsAndBinaries.Exercise4.calculate('32+96')
128 = StringsAndBinaries.Exercise4.calculate('32 + 96')
128 = StringsAndBinaries.Exercise4.calculate('256-128')
128 = StringsAndBinaries.Exercise4.calculate('256 - 128')
128 = StringsAndBinaries.Exercise4.calculate('32*4')
128 = StringsAndBinaries.Exercise4.calculate('32 * 4')
128.0 = StringsAndBinaries.Exercise4.calculate('512/4')
128.0 = StringsAndBinaries.Exercise4.calculate('512 / 4')
