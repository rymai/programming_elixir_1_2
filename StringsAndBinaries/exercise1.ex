defmodule StringsAndBinaries.Exercise1 do
  def printable?(str) do
    _printable?(str)
  end

  defp _printable?([char | tail]) when char in ?\s..?~ do
    _printable?(tail)
  end
  defp _printable?([]), do: true
  defp _printable?(_), do: false
end

true = StringsAndBinaries.Exercise1.printable?('abc')
false = StringsAndBinaries.Exercise1.printable?('abc£')
