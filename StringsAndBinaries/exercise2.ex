defmodule StringsAndBinaries.Exercise2 do
  def anagram?(word1, word2) do
    Enum.sort(word1) == Enum.sort(word2)
  end
end

true = StringsAndBinaries.Exercise2.anagram?('cat', 'act')
false = StringsAndBinaries.Exercise2.anagram?('cat', 'actor')
true = StringsAndBinaries.Exercise2.anagram?('incorporates', 'procreations')
