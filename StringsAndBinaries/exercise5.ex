defmodule StringsAndBinaries.Exercise5 do
  def center(words) do
    column_width = _column_width(words)

    Enum.map(words, &(_center_word(&1, column_width)))
    |> Enum.join("\n")
  end

  def _column_width(words) do
    Enum.map(words, &(String.length(&1)))
    |> Enum.max
  end

  defp _center_word(word, column_width) do
    word
    |> String.rjust(div(column_width + String.length(word), 2))
    |> String.ljust(column_width)
  end
end

expected1 = "  cat   \n" <> " zebra  \n" <> "  rémy  \n" <> "elephant"
true = expected1 == StringsAndBinaries.Exercise5.center(["cat", "zebra", "rémy", "elephant"])
IO.puts expected1

expected2 = "cat\n" <> "dog\n" <> "foo"
true = expected2 == StringsAndBinaries.Exercise5.center(["cat", "dog", "foo"])
IO.puts expected2
