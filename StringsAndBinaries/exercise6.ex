defmodule StringsAndBinaries.Exercise6 do
  def capitalize_sentences(sentences) do
    sentences
    |> String.split(". ")
    |> Enum.map(&(String.capitalize(&1)))
    |> Enum.join(". ")
  end
end

"Oh. A dog. Woof. " = StringsAndBinaries.Exercise6.capitalize_sentences("oh. a dog. woof. ")
