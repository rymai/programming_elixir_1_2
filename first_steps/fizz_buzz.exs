fizz_buzz = fn
  0, 0, _ -> "FizzBuzz"
  0, _, _ -> "Fizz"
  _, 0, _ -> "Buzz"
  _, _, c -> c
end

fizz_buzzing = fn(n) -> fizz_buzz.(rem(n, 3), rem(n, 5), n) end

IO.puts fizz_buzzing.(10)
IO.puts fizz_buzzing.(11)
IO.puts fizz_buzzing.(12)
IO.puts fizz_buzzing.(13)
IO.puts fizz_buzzing.(14)
IO.puts fizz_buzzing.(15)
IO.puts fizz_buzzing.(16)
